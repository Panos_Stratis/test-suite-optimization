#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/set.hpp>
#include <boost/interprocess/allocators/allocator.hpp>

namespace tso{

  typedef boost::interprocess::allocator<std::size_t , boost::interprocess::managed_shared_memory::segment_manager> shared_memory_set_allocator;

  typedef boost::interprocess::set<std::size_t, std::less<std::size_t>, shared_memory_set_allocator> shared_memory_set;
 
}
