int printtokens_main(int, char **);

#include <chrono>
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <string>
#include <stdio.h>
#include <sys/types.h>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

#include "tso.h"
#include "tso_cases.h"
#include "tso_tstcaseord.h"

//TODO Specify the following parameters.
#define MAX_NUM_THREAD 100
#define SHRD_MEM_SIZE 131072//in bytes
#define AVG_INSTR_SIZE 4//in bytes
#define L1_INSTR_CACHE_SIZE 32000//in bytes
#define LOW_THRESHOLD 0.25

#define PLAIN 1
#define RANDOM 2
#define OPTIMIZATION 3
#define OVERHEAD 4

std::map<std::string, int> const mode = {{"plain", PLAIN}, {"random", RANDOM}, {"optimization", OPTIMIZATION}, {"overhead", OVERHEAD}};

int execution_per_test_case;

float threshold;

std::string EXECUTION_CMD = "./";

std::string PINTOOL="obj-intel64/tso_bbltrack.so";

boost::mutex mtx;

std::vector<unsigned long long> executed_instructions;

std::unordered_set<std::size_t> basic_block_universe;

std::map<int, std::vector<size_t> > case_basic_block_map;

std::map<std::pair<int, int>, float> case_distances;

std::map<int, std::set<int,distance_operator> > case_similarity_map;

std::map<int, int> case_rank;

std::set<int, rank_operator> case_rank_set;

int get_rank(int case1){
  if(case_rank.count(case1) == 0){
    return 0;
  }
  return case_rank[case1];
}

float get_case_distance(int case1, int case2){
  if(case1 == case2){
    return 0.0;
  }
  if(case1 > case2){
    return case_distances.at(std::make_pair(case2, case1));
  }
  return case_distances.at(std::make_pair(case1, case2));
}

float calculate_case_distance(std::vector<std::size_t> *case1, std::vector<std::size_t> *case2){
  if(basic_block_universe.size() == 0){
    return 0.0;
  }

  std::vector<size_t> distance(basic_block_universe.size());
  std::vector<size_t>::iterator it;

  it = std::set_symmetric_difference(case1->begin(), case1->end(), case2->begin(), case2->end(), distance.begin());
  distance.resize(it-distance.begin());

  return (float)distance.size()/(float)basic_block_universe.size();
}

std::string construct_test_case_command(std::vector<std::string> execution_commands){
  std::string cmd = EXECUTION_CMD;
  for(auto const &s : execution_commands){
    if(&s != &execution_commands[0]){
      cmd += " "; 
    }
    cmd += s;
  }
  return cmd;
}

std::string construct_pin_command(std::string memory_id, std::string memory_set_id, std::string instruction_number_id, std::string case_cmd){
  std::string pin_cmd = "pin -t "+PINTOOL+" -mi "+memory_id+" -msi "+memory_set_id+" -ini "+instruction_number_id+" -- "+case_cmd;
  return pin_cmd;
}

void analysis_task(int case_id, std::string case_cmd){
  std::vector<size_t> case_basic_block_vector;
  std::string memory_id = std::to_string(case_id);
  std::string memory_set_id = memory_id+"set";
  std::string instruction_number_id = memory_id+"inst";

  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  boost::interprocess::managed_shared_memory segment(boost::interprocess::create_only, memory_id.c_str(), SHRD_MEM_SIZE);
  tso::shared_memory_set_allocator allocator(segment.get_segment_manager());
  tso::shared_memory_set* smset = segment.construct<tso::shared_memory_set>(memory_set_id.c_str())(std::less<std::size_t>(), allocator);
  unsigned long long* instruction_number = segment.construct<unsigned long long>(instruction_number_id.c_str())();

  std::system(construct_pin_command(memory_id, memory_set_id, instruction_number_id, case_cmd).c_str());
  
  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  mtx.lock();
  executed_instructions.push_back(*instruction_number);
  for (auto it=smset->begin(); it!=smset->end(); ++it){
    basic_block_universe.insert(*it);
    case_basic_block_vector.push_back(*it);
  }
  case_basic_block_map.insert(std::pair<int, std::vector<std::size_t>>(case_id, case_basic_block_vector));
  mtx.unlock();
}

void analyze_cases(){
  int thread_num = 0;
  if(test_cases.size() < MAX_NUM_THREAD){
    thread_num = test_cases.size();
  }
  else{
    thread_num = MAX_NUM_THREAD;
  }

  boost::asio::io_service ioService;
  for(auto const &case_entry : test_cases){
    ioService.post(boost::bind(analysis_task, case_entry.first, construct_test_case_command(case_entry.second)));
  }

  boost::thread_group thread_pool;
  for(int i=0; i<thread_num; i++){
    thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
  }
  thread_pool.join_all();
}

void update_similarity_map(int case1, int case2){
  if(case_rank.count(case1) == 0){
    distance_operator dop(case1);
    std::set<int, distance_operator> case_similarity_set(dop);
    case_similarity_set.insert(case2);
    case_similarity_map.insert(std::make_pair(case1, case_similarity_set));
  }
  case_rank[case1]++;
  case_similarity_map.at(case1).insert(case2);
}

void case_distance_task(int case1, std::vector<size_t> *case1_basic_blocks, int case2, std::vector<size_t> *case2_basic_blocks){
  if(case1 == case2){
    throw 0;
  }
  if(case1 > case2){
    std::swap(case1, case2);
  }

  float dist = calculate_case_distance(case1_basic_blocks, case2_basic_blocks);
  
  if(dist <= threshold){
    mtx.lock();
    case_distances.insert(std::make_pair(std::make_pair(case1, case2), dist));
    update_similarity_map(case1, case2);
    update_similarity_map(case2, case1);
    mtx.unlock();
  }
}

void calculate_threshold(){
  float cache_instruction_capacity = (float) L1_INSTR_CACHE_SIZE / (float) AVG_INSTR_SIZE;
  float average_instructions_executed;

  if(executed_instructions.size() == 0){
    average_instructions_executed = 0;
  }
  else{
    average_instructions_executed = std::accumulate(executed_instructions.begin(), executed_instructions.end(), 0.0 ) / executed_instructions.size();
  }

  threshold = 1 - (average_instructions_executed / cache_instruction_capacity);

  if(threshold < LOW_THRESHOLD){
    threshold = LOW_THRESHOLD;
  }
}

void calculate_case_distances(){
  int iteration_comparisons = case_basic_block_map.size();
  int thread_num = 0;
  calculate_threshold();
  for(auto it=case_basic_block_map.begin(); it!=case_basic_block_map.end(); ++it ){
    boost::asio::io_service ioService;
    boost::thread_group thread_pool;
    for(auto it2 = std::next(it); it2!=case_basic_block_map.end(); ++it2){
      ioService.post(boost::bind(case_distance_task, it->first, &it->second, it2->first, &it2->second));
    }
    iteration_comparisons--;
    
    if(iteration_comparisons < MAX_NUM_THREAD){
      thread_num = iteration_comparisons;
    }
    else{
      thread_num = MAX_NUM_THREAD;
    }
    for(int i=0; i<thread_num; i++){
      thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
    }
    thread_pool.join_all();
    case_rank_set.insert(it->first);
  }
  //basic_block_universe.clear();
  //case_basic_block_map.clear();
}

void visit_test_case(int test_case){
  test_order.push_back(test_case);
  case_rank_set.erase(test_case);
  if(case_rank[test_case] > 0){
    for(auto it=case_similarity_map.at(test_case).begin(); it!=case_similarity_map.at(test_case).end(); ++it){
      if(case_rank_set.count(*it) != 0){
	visit_test_case(*it);
	break;
      }
    }
  }
}

void calculate_case_order(){
  test_order.clear();
  while(!case_rank_set.empty()){
    int starting_case = *(case_rank_set.begin());
    visit_test_case(starting_case);
  }
  //case_distances.clear();
  //case_similarity_map.clear();
  //case_rank.clear();
  //case_rank_set.clear();
}

void optimize_test_suite(){
  std::chrono::system_clock::time_point start;
  std::chrono::system_clock::time_point end;
  
  start = std::chrono::system_clock::now();
  analyze_cases();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test cases analysis: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;

  start = std::chrono::system_clock::now();
  calculate_case_distances();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case distance calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
  
  start = std::chrono::system_clock::now();
  calculate_case_order();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case order calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;




  float val = 0;
  std::vector<float> dists;
  for(auto it = case_distances.begin(); it != case_distances.end(); it++) {
    dists.push_back(it->second);
  }
  for (std::vector<float>::iterator it = dists.begin() ; it != dists.end(); ++it){
    val += *it;
  }
  std::cerr<<"Average test case distance: "<<(val/dists.size())<<std::endl;

  float met = 0;
  std::vector<float> dista;
  for(auto it = case_basic_block_map.begin();it!=case_basic_block_map.end(); it++) {
    dista.push_back(((float)it->second.size() / basic_block_universe.size()));
  }
  for (std::vector<float>::iterator it = dista.begin() ; it != dista.end(); ++it){
    met += *it;
  }
  std::cerr<<"Average test case coverage:"<<(met/dista.size())<<std::endl;
}

void shuffle_test_suite(){
  std::random_shuffle(test_order.begin(), test_order.end());
}

void run_test_suite(){
  std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
  std::vector<std::string> test_arguments;
  for (std::vector<int>::iterator it = test_order.begin() ; it != test_order.end(); ++it){
    test_arguments = test_cases.at(*it);
    char **test  = new char *[test_arguments.size()];
    for(int i=0; i<test_arguments.size(); i++){
      test[i] = new char[test_arguments[i].size() + 1];
      std::strcpy(test[i], test_arguments[i].c_str());
    }
    for(int i=0; i<execution_per_test_case; i++){
      printtokens_main(test_arguments.size(), test);
    }
    for(int i=0; i<test_arguments.size(); i++){
      delete test[i];
      test[i] = NULL;
    }
    delete[] test;
    test = NULL;
  }
  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  std::cerr<<"Test suite run : "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

int main(int argc, char * argv[]){
  if(argc < 2){
    std::cerr<<"No execution mode was selected [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  if(mode.count(std::string(argv[1])) == 0){
    std::cerr<<"Invalid execution mode [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  execution_per_test_case = std::atoi(argv[2]);

  switch(mode.at(std::string(argv[1]))){
  case PLAIN :
    run_test_suite();
    break;
  case RANDOM :
    shuffle_test_suite();
    run_test_suite();
    break;
  case OPTIMIZATION :
    optimize_test_suite();
    run_test_suite();
    break;
  case OVERHEAD :
    optimize_test_suite();
    break;
    }

  return 0;
}



/*
  =====================================================================================================================
 */



#include <string.h>
# include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

# define START  5
# define TRUE  1
# define FALSE 0

typedef int BOOLEAN;
typedef char *string;

# include <stdio.h>
# include "tokens.h"

static token numeric_case();
static token error_or_eof_case();
static int check_delimiter();
static int keyword(int state);
static int special(int state);
static int skip(character_stream stream_ptr);
static int constant(int state,char token_str[],int token_ind);
static int next_state();
static int get_actual_token(char token_str[],int token_ind);

BOOLEAN print_token(token);
token get_token(token_stream);
static token numeric_case(token_stream,token,char,char[],int);
static token error_or_eof_case(token_stream,token,int,char[],int,char);
static int next_state(int,char);
static int check_delimiter(char);
BOOLEAN is_eof_token(token);
token_stream open_token_stream(string);

int printtokens_main(int argc, char * argv[])
{
      token token_ptr;
      token_stream stream_ptr;

      if(argc>2)
      {
          fprintf(stdout, "The format is print_tokens filename(optional)\n");
          return -1;
      }
      stream_ptr=open_token_stream(argv[1]);

      if(stream_ptr->ch_stream == 0)
	return 0;

      while(!is_eof_token((token_ptr=get_token(stream_ptr))))
                print_token(token_ptr);
      print_token(token_ptr);


      fclose(stream_ptr->ch_stream->fp);
return 0;
}



/* *********************************************************************
       Function name : open_character_stream
       Input         : filename 
       Output        : charactre stream.
       Exceptions    : If file name doesn't exists it will
                       exit from the program.
       Description   : The function first allocates the memory for 
                       the structure and initilizes it. The constant
                       START gives the first character available in
                       the stream. It ckecks whether the filename is
                       empty string. If it is it assigns file pointer
                       to stdin else it opens the respective file as input.                   * ******************************************************************* */

character_stream open_character_stream(string FILENAME)
{
      character_stream stream_ptr;

      stream_ptr=(character_stream)malloc(sizeof(struct stream_type));
      stream_ptr->stream_ind=START;
      stream_ptr->stream[START]='\0';
      if(FILENAME == NULL)
          stream_ptr->fp=stdin;
      else if((stream_ptr->fp=fopen(FILENAME,"r"))==NULL)
           {
               fprintf(stdout, "The file %s doesn't exists\n",FILENAME);
               return 0;
           }
      return(stream_ptr);
}

/* *********************************************************************
   Function name : get_char
   Input         : charcter_stream.
   Output        : character.
   Exceptions    : None.
   Description   : This function takes character_stream type variable 
                   as input and returns one character. If the stream is
                   empty then it reads the next line from the file and
                   returns the character.       
 * ****************************************************************** */

CHARACTER get_char(character_stream stream_ptr)
{
      if(stream_ptr->stream[stream_ptr->stream_ind] == '\0')
      {
              if(fgets(stream_ptr->stream+START,80-START,stream_ptr->fp) == NULL)/* Fix bug: add -START - hf*/
                    stream_ptr->stream[START]=EOF;
              stream_ptr->stream_ind=START;
      }
      return(stream_ptr->stream[(stream_ptr->stream_ind)++]);
}

/* *******************************************************************
   Function name : is_end_of_character_stream.
   Input         : character_stream.
   Output        : Boolean value.
   Description   : This function checks whether it is end of character
                   stream or not. It returns BOOLEANvariable which is 
                   true or false. The function checks whether the last 
                   read character is end file character or not and
                   returns the value according to it.
 * ****************************************************************** */

BOOLEAN is_end_of_character_stream(character_stream stream_ptr)
{
      if(stream_ptr->stream[stream_ptr->stream_ind-1] == EOF)
            return(TRUE);
      else
            return(FALSE);
}

/* *********************************************************************
   Function name : unget_char
   Input         : character,character_stream.
   Output        : void.
   Description   : This function adds the character ch to the stream. 
                   This is accomplished by decrementing the stream_ind
                   and storing it in the stream. If it is not possible
                   to unget the character then it returns
 * ******************************************************************* */

int unget_char(CHARACTER ch,character_stream stream_ptr)
{
      if(stream_ptr->stream_ind == 0)
          return 0;
      else
          stream_ptr->stream[--(stream_ptr->stream_ind)]=ch;
      return 0;
}


/* *******************************************************************
   Function name : open_token_stream
   Input         : filename
   Output        : token_stream
   Exceptions    : Exits if the file specified by filename not found.
   Description   : This function takes filename as input and opens the
                   token_stream which is nothing but the character stream.
                   This function allocates the memory for token_stream 
                   and calls open_character_stream to open the file as
                   input. This function returns the token_stream.
 * ****************************************************************** */

token_stream open_token_stream(string FILENAME)
{
    token_stream token_ptr;
  
    token_ptr=(token_stream)malloc(sizeof(struct token_stream_type));
    token_ptr->ch_stream=open_character_stream(FILENAME);/* Get character
                                                             stream  */
    return(token_ptr);
}

/* ********************************************************************
   Function name : get_token
   Input         : token_stream
   Output        : token
   Exceptions    : none.
   Description   : This function returns the next token from the
                   token_stream.The type of token is integer and specifies 
                   only the type of the token. DFA is used for finding the
                   next token. cu_state is initialized to zero and charcter
                   are read until the the is the final state and it
                   returns the token type.
* ******************************************************************* */

token get_token(token_stream tstream_ptr)
{
      char token_str[80]; /* This buffer stores the current token */
      int token_ind;      /* Index to the token_str  */
      token token_ptr;
      CHARACTER ch;
      int cu_state,next_st,token_found;
  
      token_ptr=(token)(malloc(sizeof(struct token_type)));
      ch=get_char(tstream_ptr->ch_stream);
      cu_state=token_ind=token_found=0;
      while(!token_found)
      {
	  if(token_ind < 80) /* ADDED ERROR CHECK - hf */
	  {
	      token_str[token_ind++]=ch;
	      next_st=next_state(cu_state,ch);
	  }
	  else
	  {
	      next_st = -1; /* - hf */
	  }
	  if (next_st == -1) { /* ERROR or EOF case */
	      return(error_or_eof_case(tstream_ptr, 
				       token_ptr,cu_state,token_str,token_ind,ch));
	  } else if (next_st == -2) {/* This is numeric case. */
	      return(numeric_case(tstream_ptr,token_ptr,ch,
				  token_str,token_ind));
	  } else if (next_st == -3) {/* This is the IDENTIFIER case */
	      token_ptr->token_id=IDENTIFIER;
	      unget_char(ch,tstream_ptr->ch_stream);
	      token_ind--;
	      get_actual_token(token_str,token_ind);
	      strcpy(token_ptr->token_string,token_str);
	      return(token_ptr);
	  } 
	    
	  switch(next_st) 
            { 
                 default : break;
                 case 6  : /* These are all KEYWORD cases. */
                 case 9  :
                 case 11 :
                 case 13 :
                 case 16 :
                 case 32 : ch=get_char(tstream_ptr->ch_stream);
                           if(check_delimiter(ch)==TRUE)
                           {
                                 token_ptr->token_id=keyword(next_st);
                                 unget_char(ch,tstream_ptr->ch_stream);
                                 token_ptr->token_string[0]='\0';
                                 return(token_ptr);
                           }
                           unget_char(ch,tstream_ptr->ch_stream);
                           break;
                 case 19 : /* These are all special SPECIAL character */
                 case 20 : /* cases */
                 case 21 :
                 case 22 :
                 case 23 :
                 case 24 :
                 case 25 : token_ptr->token_id=special(next_st);
                           token_ptr->token_string[0]='\0';
                           return(token_ptr);
                 case 27 : /* These are constant cases */
                 case 29 : token_ptr->token_id=constant(next_st,token_str,token_ind);
                           get_actual_token(token_str,token_ind);
                           strcpy(token_ptr->token_string,token_str);
                           return(token_ptr);
                 case 30 :  /* This is COMMENT case */
                           skip(tstream_ptr->ch_stream);
                           token_ind=next_st=0;
                           break;
            }
            cu_state=next_st;
            ch=get_char(tstream_ptr->ch_stream);
      }
}

/* ******************************************************************
   Function name : numeric_case
   Input         : tstream_ptr,token_ptr,ch,token_str,token_ind
   Output        : token_ptr;
   Exceptions    : none 
   Description   : It checks for the delimiter, if it is then it
                   forms numeric token else forms error token.
 * ****************************************************************** */

static token numeric_case(token_stream tstream_ptr,token token_ptr,char ch,char token_str[],int token_ind)
{
        if(check_delimiter(ch)!=TRUE)
        {   /* Error case */
            token_ptr->token_id=ERROR;
            while(check_delimiter(ch)==FALSE)
	    {
		if(token_ind >= 80) break; /* Added protection - hf */
		token_str[token_ind++]=ch=get_char(tstream_ptr->ch_stream);
	    }
            unget_char(ch,tstream_ptr->ch_stream);
            token_ind--;
            get_actual_token(token_str,token_ind);
            strcpy(token_ptr->token_string,token_str);
            return(token_ptr);
        }
        token_ptr->token_id=NUMERIC; /* Numeric case */
        unget_char(ch,tstream_ptr->ch_stream);
        token_ind--;
        get_actual_token(token_str,token_ind);
        strcpy(token_ptr->token_string,token_str);
        return(token_ptr);
}

/* *****************************************************************
   Function name : error_or_eof_case 
   Input         : tstream_ptr,token_ptr,cu_state,token_str,token_ind,ch
   Output        : token_ptr 
   Exceptions    : none 
   Description   : This function checks whether it is EOF or not.
                   If it is it returns EOF token else returns ERROR 
                   token.
 * *****************************************************************/

static token error_or_eof_case(token_stream tstream_ptr,token token_ptr,int cu_state,char token_str[],int token_ind,char ch)
{
      if(is_end_of_character_stream(tstream_ptr->ch_stream)) 
      {
            token_ptr->token_id = EOTSTREAM;
            token_ptr->token_string[0]='\0';
            return(token_ptr);
      }
      if(cu_state !=0)
      {
            unget_char(ch,tstream_ptr->ch_stream);
            token_ind--;
      }
      token_ptr->token_id=ERROR;
      get_actual_token(token_str,token_ind);
      strcpy(token_ptr->token_string,token_str);
      return(token_ptr);                
}

/* *********************************************************************
   Function name : check_delimiter
   Input         : character
   Output        : boolean
   Exceptions    : none.
   Description   : This function checks for the delimiter. If ch is not
                   alphabet and non numeric then it returns TRUE else 
                   it returns FALSE. 
 * ******************************************************************* */

static int check_delimiter(char ch)
{
      if(!isalpha(ch) && !isdigit(ch)) /* Check for digit and alpha */
          return(TRUE);
      return(FALSE);
}

/* ********************************************************************
   Function name : keyword
   Input         : state of the DFA
   Output        : Keyword.
   Exceptions    : If the state doesn't represent a keyword it exits.
   Description   : According to the final state specified by state the
                   respective token_id is returned.
 * ***************************************************************** */

static int keyword(int state)
{
      switch(state)
      {   /* Return the respective macro for the Keyword. */
          case 6 : return(LAMBDA);
          case 9 : return(AND);
          case 11: return(OR);
          case 13: return(IF);
          case 16: return(XOR);
          case 32: return(EQUALGREATER);
          default: fprintf(stdout, "error\n");break;
      }
      return 0;
}

/* ********************************************************************
   Function name : special
   Input         : The state of the DFA.
   Output        : special symbol.
   Exceptions    : if the state doesn't belong to a special character
                   it exits.
   Description   : This function returns the token_id according to the
                   final state given by state.
 * ****************************************************************** */

static int special(int state)
{
     switch(state)
     {   /* return the respective macro for the special character. */
         case 19: return(LPAREN);
         case 20: return(RPAREN);
         case 21: return(LSQUARE);
         case 22: return(RSQUARE);
         case 23: return(QUOTE);
         case 24: return(BQUOTE);
         case 25: return(COMMA);
         default: fprintf(stdout, "error\n");break;
     }
     return 0;
}

/* **********************************************************************
   Function name : skip
   Input         : character_stream
   Output        : void.
   Exceptions    : none.
   Description   : This function skips the comment part of the program.
                   It takes charcter_stream as input and reads character
                   until it finds new line character or
                   end_of_character_stream.                   
 * ******************************************************************* */

static int skip(character_stream stream_ptr)
{
        char c;
  
        while((c=get_char(stream_ptr))!='\n' && 
               !is_end_of_character_stream(stream_ptr))
             ; /* Skip the characters until EOF or EOL found. */
	if(c==EOF) unget_char(c, stream_ptr); /* Put back to leave gracefully - hf */
        return 0;
}

/* *********************************************************************
   Function name : constant
   Input         : state of DFA, Token string, Token id.
   Output        : constant token.
   Exceptions    : none.
   Description   : This function returns the token_id for the constatnts
                   speccified by  the final state. 
 * ****************************************************************** */

static int constant(int state,char token_str[],int token_ind)
{
     switch(state)
     {   /* Return the respective CONSTANT macro. */
         case 27 : return(STRING_CONSTANT);
         case 29 : token_str[token_ind-2]=' '; return(CHARACTER_CONSTANT);
         default : break;
     }
}


/* *******************************************************************
   Function name : next_state
   Input         : current state, character
   Output        : next state of the DFA
   Exceptions    : none.
   Description   : This function returns the next state in the transition
                   diagram. The next state is determined by the current
                   state state and the inpu character ch.
 * ****************************************************************** */
              
static int next_state(int state,char ch)
{
    if(state < 0)
      return(state);
    if(base[state]+ch >= 0)
    {
        if(check[base[state]+ch] == state) /* Check for the right state */
             return(next[base[state]+ch]);
        else
              return(next_state(default1[state],ch));
    }
    else
        return(next_state(default1[state],ch));
}

/* *********************************************************************
   Function name : is_eof_token
   Input         : token
   Output        : Boolean
   Exceptions    : none.
   Description   : This function checks whether the token t is eof_token 
                   or not. If the integer value stored in the t is
                   EOTSTREAM then it is eof_token.
 * ***************************************************************** */

BOOLEAN is_eof_token(token t)
{
    if(t->token_id==EOTSTREAM)
        return(TRUE);
    return(FALSE);
}

/* ********************************************************************
   Function name : print_token
   Input         : token
   Output        : Boolean
   Exceptions    : none.
   Description   : This function  prints the token. The token_id gives 
                   the type of token not the token itself. So, in the
                   case of identifier,numeric,  string,character it is
                   required to print the actual token  from token_str. 
                   So, precaution must be taken when printing the token.
                   This function is able to print the current token only
                   and it is the limitation of the program.
 * ******************************************************************** */

BOOLEAN print_token(token token_ptr)
{
     switch(token_ptr->token_id)
     {    /* Print the respective tokens. */
          case ERROR : fprintf(stdout, "error,\t\"");fprintf(stdout, "%s",token_ptr->token_string);
                       fprintf(stdout, "\".\n");return(TRUE);
          case EOTSTREAM : fprintf(stdout, "eof.\n");return(TRUE);
          case 6 : fprintf(stdout, "keyword,\t\"lambda\".\n");return(TRUE);
          case 9 : fprintf(stdout, "keyword,\t\"and\".\n");return(TRUE);
          case 11: fprintf(stdout, "keyword,\t\"or\".\n");return(TRUE);
          case 13: fprintf(stdout, "keyword,\t\"if\".\n");return(TRUE);
          case 16: fprintf(stdout, "keyword,\t\"xor\".\n");return(TRUE);
          case 17: fprintf(stdout, "identifier,\t\"");fprintf(stdout, "%s",token_ptr->token_string);
                   fprintf(stdout, "\".\n");return(TRUE);
          case 18: fprintf(stdout, "numeric,\t");fprintf(stdout, "%s",token_ptr->token_string);
                   fprintf(stdout, ".\n");return(TRUE);
          case 19: fprintf(stdout, "lparen.\n");return(TRUE);
          case 20: fprintf(stdout, "rparen.\n");return(TRUE);
          case 21: fprintf(stdout, "lsquare.\n");return(TRUE);
          case 22: fprintf(stdout, "rsquare.\n");return(TRUE);
          case 23: fprintf(stdout, "quote.\n");return(TRUE);
          case 24: fprintf(stdout, "bquote.\n");return(TRUE);
          case 25: fprintf(stdout, "comma.\n");return(TRUE);
          case 27: fprintf(stdout, "string,\t");fprintf(stdout, "%s",token_ptr->token_string);
                   fprintf(stdout, ".\n");return(TRUE);
          case 29: fprintf(stdout, "character,\t\"");fprintf(stdout, "%s",token_ptr->token_string);
                   fprintf(stdout, "\".\n");return(TRUE);
          case 32: fprintf(stdout, "keyword,\t\"=>\".\n");return(TRUE);
          default: break;
      }
      return(FALSE);
}

/* **********************************************************************
   Function name : get_actual_token
   Input         : token string and token id.
   Output        : void.
   Exceptions    : none.
   Description   : This function prints the actual token in the case of
                   identifier,numeric,string and character. It removes
                   the leading and trailing  spaces and prints the token.
 * ****************************************************************** */

static int get_actual_token(char token_str[],int token_ind)
{
          int ind,start;

          for(ind=token_ind;ind>0 && isspace(token_str[ind-1]);--ind); 
                        /* Delete the trailing white spaces & protect - hf */
           token_str[ind]='\0';token_ind=ind;
          for(ind=0;ind<token_ind;++ind)
                if(!isspace(token_str[ind]))
                      break;
          for(start=0;ind<=token_ind;++start,++ind) /* Delete the leading
                                                       white spaces. */
                token_str[start]=token_str[ind];
          return 0;
}
