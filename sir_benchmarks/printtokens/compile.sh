#!/bin/bash

[ -d printtokens ] && rm -r printtokens
tar zxvf printtokens_2.0.tar.gz

cd printtokens/source
cp ../../source/* .
clang++ -std=c++11 -g print_tokens.cpp -o print_tokens
make CXX="g++ -I $boost -std=c++11" DWARF_LIBS="-lpindwarf -lboost_thread -lboost_system -lpthread -lrt"  PIN_ROOT=$pin_root obj-intel64/tso_bbltrack.so
clang++ -I $boost -std=c++11 tso_tstcaseord.cpp -o bmark -lboost_thread -lboost_system -lpthread -lrt
cd ../..
