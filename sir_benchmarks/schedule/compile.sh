#!/bin/bash

[ -d schedule ] && rm -r schedule
tar zxvf schedule_2.0.tar.gz

cd schedule/source
cp ../../source/* .
clang++ -std=c++11 -g schedule.cpp -o schedule
make CXX="g++ -I $boost -std=c++11" DWARF_LIBS="-lpindwarf -lboost_thread -lboost_system -lpthread -lrt"  PIN_ROOT=$pin_root obj-intel64/tso_bbltrack.so
clang++ -I $boost -std=c++11 tso_tstcaseord.cpp -o bmark -lboost_thread -lboost_system -lpthread -lrt
cd ../..
