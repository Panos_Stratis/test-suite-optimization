int schedule_main(int, char **);

#include <chrono>
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <string>
#include <stdio.h>
#include <sys/types.h>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

#include "tso.h"
#include "tso_cases.h"
#include "tso_tstcaseord.h"

//TODO Specify the following parameters.
#define MAX_NUM_THREAD 100
#define SHRD_MEM_SIZE 131072//in bytes
#define AVG_INSTR_SIZE 4//in bytes
#define L1_INSTR_CACHE_SIZE 32000//in bytes
#define LOW_THRESHOLD 0.25

#define PLAIN 1
#define RANDOM 2
#define OPTIMIZATION 3
#define OVERHEAD 4

std::map<std::string, int> const mode = {{"plain", PLAIN}, {"random", RANDOM}, {"optimization", OPTIMIZATION}, {"overhead", OVERHEAD}};

int execution_per_test_case;

float threshold;

std::string EXECUTION_CMD = "./";

std::string PINTOOL="obj-intel64/tso_bbltrack.so";

boost::mutex mtx;

std::vector<unsigned long long> executed_instructions;

std::unordered_set<std::size_t> basic_block_universe;

std::map<int, std::vector<size_t> > case_basic_block_map;

std::map<std::pair<int, int>, float> case_distances;

std::map<int, std::set<int,distance_operator> > case_similarity_map;

std::map<int, int> case_rank;

std::set<int, rank_operator> case_rank_set;

int get_rank(int case1){
  if(case_rank.count(case1) == 0){
    return 0;
  }
  return case_rank[case1];
}

float get_case_distance(int case1, int case2){
  if(case1 == case2){
    return 0.0;
  }
  if(case1 > case2){
    return case_distances.at(std::make_pair(case2, case1));
  }
  return case_distances.at(std::make_pair(case1, case2));
}

float calculate_case_distance(std::vector<std::size_t> *case1, std::vector<std::size_t> *case2){
  if(basic_block_universe.size() == 0){
    return 0.0;
  }

  std::vector<size_t> distance(basic_block_universe.size());
  std::vector<size_t>::iterator it;

  it = std::set_symmetric_difference(case1->begin(), case1->end(), case2->begin(), case2->end(), distance.begin());
  distance.resize(it-distance.begin());

  return (float)distance.size()/(float)basic_block_universe.size();
}

std::string construct_test_case_command(std::vector<std::string> execution_commands){
  std::string cmd = EXECUTION_CMD;
  for(auto const &s : execution_commands){
    if(&s != &execution_commands[0]){
      cmd += " "; 
    }
    cmd += s;
  }
  return cmd;
}

std::string construct_pin_command(std::string memory_id, std::string memory_set_id, std::string instruction_number_id, std::string case_cmd){
  std::string pin_cmd = "pin -t "+PINTOOL+" -mi "+memory_id+" -msi "+memory_set_id+" -ini "+instruction_number_id+" -- "+case_cmd;
  return pin_cmd;
}

void analysis_task(int case_id, std::string case_cmd){
  std::vector<size_t> case_basic_block_vector;
  std::string memory_id = std::to_string(case_id);
  std::string memory_set_id = memory_id+"set";
  std::string instruction_number_id = memory_id+"inst";

  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  boost::interprocess::managed_shared_memory segment(boost::interprocess::create_only, memory_id.c_str(), SHRD_MEM_SIZE);
  tso::shared_memory_set_allocator allocator(segment.get_segment_manager());
  tso::shared_memory_set* smset = segment.construct<tso::shared_memory_set>(memory_set_id.c_str())(std::less<std::size_t>(), allocator);
  unsigned long long* instruction_number = segment.construct<unsigned long long>(instruction_number_id.c_str())();

  std::system(construct_pin_command(memory_id, memory_set_id, instruction_number_id, case_cmd).c_str());
  
  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  mtx.lock();
  executed_instructions.push_back(*instruction_number);
  for (auto it=smset->begin(); it!=smset->end(); ++it){
    basic_block_universe.insert(*it);
    case_basic_block_vector.push_back(*it);
  }
  case_basic_block_map.insert(std::pair<int, std::vector<std::size_t>>(case_id, case_basic_block_vector));
  mtx.unlock();
}

void analyze_cases(){
  int thread_num = 0;
  if(test_cases.size() < MAX_NUM_THREAD){
    thread_num = test_cases.size();
  }
  else{
    thread_num = MAX_NUM_THREAD;
  }

  boost::asio::io_service ioService;
  for(auto const &case_entry : test_cases){
    ioService.post(boost::bind(analysis_task, case_entry.first, construct_test_case_command(case_entry.second)));
  }

  boost::thread_group thread_pool;
  for(int i=0; i<thread_num; i++){
    thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
  }
  thread_pool.join_all();
}

void update_similarity_map(int case1, int case2){
  if(case_rank.count(case1) == 0){
    distance_operator dop(case1);
    std::set<int, distance_operator> case_similarity_set(dop);
    case_similarity_set.insert(case2);
    case_similarity_map.insert(std::make_pair(case1, case_similarity_set));
  }
  case_rank[case1]++;
  case_similarity_map.at(case1).insert(case2);
}

void case_distance_task(int case1, std::vector<size_t> *case1_basic_blocks, int case2, std::vector<size_t> *case2_basic_blocks){
  if(case1 == case2){
    throw 0;
  }
  if(case1 > case2){
    std::swap(case1, case2);
  }

  float dist = calculate_case_distance(case1_basic_blocks, case2_basic_blocks);
  
  if(dist <= threshold){
    mtx.lock();
    case_distances.insert(std::make_pair(std::make_pair(case1, case2), dist));
    update_similarity_map(case1, case2);
    update_similarity_map(case2, case1);
    mtx.unlock();
  }
}

void calculate_threshold(){
  float cache_instruction_capacity = (float) L1_INSTR_CACHE_SIZE / (float) AVG_INSTR_SIZE;
  float average_instructions_executed;

  if(executed_instructions.size() == 0){
    average_instructions_executed = 0;
  }
  else{
    average_instructions_executed = std::accumulate(executed_instructions.begin(), executed_instructions.end(), 0.0 ) / executed_instructions.size();
  }

  threshold = 1 - (average_instructions_executed / cache_instruction_capacity);

  if(threshold < LOW_THRESHOLD){
    threshold = LOW_THRESHOLD;
  }
}

void calculate_case_distances(){
  int iteration_comparisons = case_basic_block_map.size();
  int thread_num = 0;
  calculate_threshold();
  for(auto it=case_basic_block_map.begin(); it!=case_basic_block_map.end(); ++it ){
    boost::asio::io_service ioService;
    boost::thread_group thread_pool;
    for(auto it2 = std::next(it); it2!=case_basic_block_map.end(); ++it2){
      ioService.post(boost::bind(case_distance_task, it->first, &it->second, it2->first, &it2->second));
    }
    iteration_comparisons--;
    
    if(iteration_comparisons < MAX_NUM_THREAD){
      thread_num = iteration_comparisons;
    }
    else{
      thread_num = MAX_NUM_THREAD;
    }
    for(int i=0; i<thread_num; i++){
      thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
    }
    thread_pool.join_all();
    case_rank_set.insert(it->first);
  }
  basic_block_universe.clear();
  case_basic_block_map.clear();
}

void visit_test_case(int test_case){
  test_order.push_back(test_case);
  case_rank_set.erase(test_case);
  if(case_rank[test_case] > 0){
    for(auto it=case_similarity_map.at(test_case).begin(); it!=case_similarity_map.at(test_case).end(); ++it){
      if(case_rank_set.count(*it) != 0){
	visit_test_case(*it);
	break;
      }
    }
  }
}

void calculate_case_order(){
  test_order.clear();
  while(!case_rank_set.empty()){
    int starting_case = *(case_rank_set.begin());
    visit_test_case(starting_case);
  }
  case_distances.clear();
  case_similarity_map.clear();
  case_rank.clear();
  case_rank_set.clear();
}

void optimize_test_suite(){
  std::chrono::system_clock::time_point start;
  std::chrono::system_clock::time_point end;
  
  start = std::chrono::system_clock::now();
  analyze_cases();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test cases analysis: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;

  start = std::chrono::system_clock::now();
  calculate_case_distances();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case distance calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
  
  start = std::chrono::system_clock::now();
  calculate_case_order();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case order calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

void shuffle_test_suite(){
  std::random_shuffle(test_order.begin(), test_order.end());
}

void run_test_suite(){
  std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
  std::vector<std::string> test_arguments;
  int counter = -1;
  for (std::vector<int>::iterator it = test_order.begin() ; it != test_order.end(); ++it){
    counter ++;
    test_arguments = test_cases.at(*it);
    char **test  = new char *[test_arguments.size()];
    for(int i=0; i<test_arguments.size(); i++){
      test[i] = new char[test_arguments[i].size()+1];
      std::strcpy(test[i], test_arguments[i].c_str());
    }
    for(int i=0; i<execution_per_test_case; i++){
      schedule_main(test_arguments.size(), test);
    }
    for(int i=0; i<test_arguments.size(); i++){
      delete test[i];
      test[i] = NULL;
    }
    delete[] test;
    test = NULL;
  }
  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  std::cerr<<"Test suite run : "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

int main(int argc, char * argv[]){
  if(argc < 2){
    std::cerr<<"No execution mode was selected [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  if(mode.count(std::string(argv[1])) == 0){
    std::cerr<<"Invalid execution mode [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  execution_per_test_case = std::atoi(argv[2]);

  switch(mode.at(std::string(argv[1]))){
  case PLAIN :
    run_test_suite();
    break;
  case RANDOM :
    shuffle_test_suite();
    run_test_suite();
    break;
  case OPTIMIZATION :
    optimize_test_suite();
    run_test_suite();
    break;
  case OVERHEAD :
    optimize_test_suite();
    break;
    }

  return 0;
}



/*
  =====================================================================================================================
 */





/*  -*- Last-Edit:  Wed May 7 10:12:52 1993 by Monica; -*- */

#include <stdlib.h>
#include <stdio.h>

/* A job descriptor. */

#define NULLL 0


#define NEW_JOB        1
#define UPGRADE_PRIO   2 
#define BLOCK          3
#define UNBLOCK        4  
#define QUANTUM_EXPIRE 5
#define FINISH         6
#define FLUSH          7

#define MAXPRIO 3

typedef struct _job {
    struct  _job *next, *prev; /* Next and Previous in job list. */
    int          val  ;         /* Id-value of program. */
    short        priority;     /* Its priority. */
} Ele, *Ele_Ptr;

typedef struct list		/* doubly linked list */
{
  Ele *first;
  Ele *last;
  int    mem_count;		/* member count */
} List;

int schedule();

/*-----------------------------------------------------------------------------
  new_ele
     alloates a new element with value as num.
-----------------------------------------------------------------------------*/
Ele* new_ele(int new_num) 
{	
    Ele *ele;

    ele =(Ele *)malloc(sizeof(Ele));
    ele->next = NULLL;
    ele->prev = NULLL;
    ele->val  = new_num;
    return ele;
}

/*-----------------------------------------------------------------------------
  new_list
        allocates, initializes and returns a new list.
        Note that if the argument compare() is provided, this list can be
            made into an ordered list. see insert_ele().
-----------------------------------------------------------------------------*/
List *new_list()
{
    List *list;

    list = (List *)malloc(sizeof(List));
    
    list->first = NULLL;
    list->last  = NULLL;
    list->mem_count = 0;
    return (list);
}

/*-----------------------------------------------------------------------------
  append_ele
        appends the new_ele to the list. If list is null, a new
	list is created. The modified list is returned.
-----------------------------------------------------------------------------*/
List *append_ele(List *a_list, Ele *a_ele)
{
  if (!a_list)
      a_list = new_list();	/* make list without compare function */

  a_ele->prev = a_list->last;	/* insert at the tail */
  if (a_list->last)
    a_list->last->next = a_ele;
  else
    a_list->first = a_ele;
  a_list->last = a_ele;
  a_ele->next = NULLL;
  a_list->mem_count++;
  return (a_list);
}

/*-----------------------------------------------------------------------------
  find_nth
        fetches the nth element of the list (count starts at 1)
-----------------------------------------------------------------------------*/
Ele *find_nth(List *f_list, int n)
{
    Ele *f_ele;
    int i;

    if (!f_list)
	return NULLL;
    f_ele = f_list->first;
    for (i=1; f_list->first && (i<n); i++){ /* logic error */
      if(f_ele == f_list->last)
	return NULLL;
      f_ele = f_ele->next;
    }
    return f_ele;
}

/*-----------------------------------------------------------------------------
  del_ele
        deletes the old_ele from the list.
        Note: even if list becomes empty after deletion, the list
	      node is not deallocated.
-----------------------------------------------------------------------------*/
List *del_ele(List *d_list, Ele *d_ele)
{
    if (!d_list || !d_ele)
	return (NULLL);
    
    if (d_ele->next)
	d_ele->next->prev = d_ele->prev;
    else
	d_list->last = d_ele->prev;
    if (d_ele->prev)
	d_ele->prev->next = d_ele->next;
    else
	d_list->first = d_ele->next;
    /* KEEP d_ele's data & pointers intact!! */
    d_list->mem_count--;
    return (d_list);
}

/*-----------------------------------------------------------------------------
   free_ele
       deallocate the ptr. Caution: The ptr should point to an object
       allocated in a single call to malloc.
-----------------------------------------------------------------------------*/
void free_ele(Ele *ptr)
{
    free(ptr);
}

int alloc_proc_num;
int num_processes;
Ele* cur_proc;
List *prio_queue[MAXPRIO+1]; 	/* 0th element unused */
List *block_queue;

void
finish_process()
{
    schedule();
    if (cur_proc)
    {
	fprintf(stdout, "%d ", cur_proc->val);
	free_ele(cur_proc);
	num_processes--;
    }
}

void
finish_all_processes()
{
    int i;
    int total;
    total = num_processes;
    for (i=0; i<total; i++)
	finish_process();
}

int schedule()
{
    int i;
    
    cur_proc = NULLL;
    for (i=MAXPRIO; i > 0; i--)
    {
	if (prio_queue[i]->mem_count > 0)
	{
	    cur_proc = prio_queue[i]->first;
	    prio_queue[i] = del_ele(prio_queue[i], cur_proc);
	    return 0;
	}
    }
    return 0;
}

void
upgrade_process_prio(int prio, float ratio)
{
    int count;
    int n;
    Ele *proc;
    List *src_queue, *dest_queue;
    
    if (prio >= MAXPRIO)
	return;
    src_queue = prio_queue[prio];
    dest_queue = prio_queue[prio+1];
    count = src_queue->mem_count;

    if (count > 0)
    {
	n = (int) (count*ratio + 1);
	proc = find_nth(src_queue, n);
	if (proc) {
	    src_queue = del_ele(src_queue, proc);
	    /* append to appropriate prio queue */
	    proc->priority = prio;
	    dest_queue = append_ele(dest_queue, proc);
	}
    }
}

void
unblock_process(float ratio)
{
    int count;
    int n;
    Ele *proc;
    int prio;
    if (block_queue)
    {
	count = block_queue->mem_count;
	n = (int) (count*ratio + 1);
	proc = find_nth(block_queue, n);
	if (proc) {
	    block_queue = del_ele(block_queue, proc);
	    /* append to appropriate prio queue */
	    prio = proc->priority;
	    prio_queue[prio] = append_ele(prio_queue[prio], proc);
	}
    }
}

void quantum_expire()
{
    int prio;
    schedule();
    if (cur_proc)
    {
	prio = cur_proc->priority;
	prio_queue[prio] = append_ele(prio_queue[prio], cur_proc);
    }	
}
	
void
block_process()
{
    schedule();
    if (cur_proc)
    {
	block_queue = append_ele(block_queue, cur_proc);
    }
}

Ele * new_process(int prio)
{
    Ele *proc;
    proc = new_ele(alloc_proc_num++);
    proc->priority = prio;
    num_processes++;
    return proc;
}

void add_process(int prio)
{
    Ele *proc;
    proc = new_process(prio);
    prio_queue[prio] = append_ele(prio_queue[prio], proc);
}

void init_prio_queue(int prio, int num_proc)
{
    List *queue;
    Ele  *proc;
    int i;
    
    queue = new_list();
    for (i=0; i<num_proc; i++)
    {
	proc = new_process(prio);
	queue = append_ele(queue, proc);
    }
    prio_queue[prio] = queue;
}

void initialize()
{
    alloc_proc_num = 0;
    num_processes = 0;
}
				
/* test driver */
int schedule_main(int argc, char *argv[])
{
    int command;
    int prio;
    float ratio;
    int status;

    FILE *fd;

    if((fd = fopen(argv[4], "r+")) == NULL)
      {
	fprintf(stdout,"No such file\n");
	return 0;
      }  

    if (fd == NULL)
      {
	fprintf(stdout,"Error Reading File\n");
	return 0;
      }

    if (argc < (MAXPRIO+1))
    {
	fprintf(stdout, "incorrect usage\n");
	return 0;
    }
    
    initialize();
    for (prio=MAXPRIO; prio >= 1; prio--)
    {
	init_prio_queue(prio, atoi(argv[prio]));
    }
    for (status = fscanf(fd, "%d", &command);
	 ((status!=EOF) && status);
	 status = fscanf(fd, "%d", &command))
    {
	switch(command)
	{
	case FINISH:
	    finish_process();
	    break;
	case BLOCK:
	    block_process();
	    break;
	case QUANTUM_EXPIRE:
	    quantum_expire();
	    break;
	case UNBLOCK:
	    fscanf(fd, "%f", &ratio);
	    unblock_process(ratio);
	    break;
	case UPGRADE_PRIO:
	    fscanf(fd, "%d", &prio);
	    fscanf(fd, "%f", &ratio);
	    if (prio > MAXPRIO || prio <= 0) { 
		fprintf(stdout, "** invalid priority\n");
		return 0;
	    }
	    else 
		upgrade_process_prio(prio, ratio);
	    break;
	case NEW_JOB:
	    fscanf(fd, "%d", &prio);
	    if (prio > MAXPRIO || prio <= 0) {
		fprintf(stdout, "** invalid priority\n");
		return 0;
	    }
	    else 
		add_process(prio);
	    break;
	case FLUSH:
	    finish_all_processes();
	    break;
	}
    }
    fclose(fd);
    return 0;
}

/* A simple input spec:
  
  a.out n3 n2 n1

  where n3, n2, n1 are non-negative integers indicating the number of
  initial processes at priority 3, 2, and 1, respectively.

  The input file is a list of commands of the following kinds:
   (For simplicity, comamnd names are integers (NOT strings)
    
  FINISH            ;; this exits the current process (printing its number)
  NEW_JOB priority  ;; this adds a new process at specified priority
  BLOCK             ;; this adds the current process to the blocked queue
  QUANTUM_EXPIRE    ;; this puts the current process at the end
                    ;;      of its prioqueue
  UNBLOCK ratio     ;; this unblocks a process from the blocked queue
                    ;;     and ratio is used to determine which one

  UPGRADE_PRIO small-priority ratio ;; this promotes a process from
                    ;; the small-priority queue to the next higher priority
                    ;;     and ratio is used to determine which process
 
  FLUSH	            ;; causes all the processes from the prio queues to
                    ;;    exit the system in their priority order

where
 NEW_JOB        1
 UPGRADE_PRIO   2 
 BLOCK          3
 UNBLOCK        4  
 QUANTUM_EXPIRE 5
 FINISH         6
 FLUSH          7
and priority is in        1..3
and small-priority is in  1..2
and ratio is in           0.0..1.0

 The output is a list of numbers indicating the order in which
 processes exit from the system.   

*/


