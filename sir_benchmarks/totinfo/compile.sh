#!/bin/bash

[ -d totinfo ] && rm -r totinfo
tar zxvf totinfo_2.0.tar.gz

cd totinfo/source
cp ../../source/* .
clang++ -std=c++11 -g tot_info.cpp -o tot_info
make CXX="g++ -I $boost -std=c++11" DWARF_LIBS="-lpindwarf -lboost_thread -lboost_system -lpthread -lrt"  PIN_ROOT=$pin_root obj-intel64/tso_bbltrack.so
clang++ -I $boost -std=c++11 tso_tstcaseord.cpp -o bmark -lboost_thread -lboost_system -lpthread -lrt
cd ../..
