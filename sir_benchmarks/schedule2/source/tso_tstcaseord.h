int get_rank(int);

float get_case_distance(int, int);

float calculate_case_distance(std::vector<std::size_t> *, std::vector<std::size_t> *);

std::string construct_test_case_command(std::vector<std::string>);

std::string construct_pin_command(std::string, std::string, std::string, std::string);

void analysis_task(int, std::string);

void analyze_cases();

void update_similarity_map(int, int);

void case_distance_task(int, std::vector<size_t> *, int, std::vector<size_t> *);

void calculate_threshold();

void calculate_case_distances();

void visit_test_case(int);

void calculate_case_order();

void optimize_test_suite();

void shuffle_test_suite();

void run_test_suite();

int main(int, char **);

struct distance_operator{
  distance_operator(int base_case){
    this->base_case = base_case;
  }
  bool operator()(int lhs, int rhs)const{
    if(lhs == rhs){
      return false;
    }
    if(get_case_distance(base_case, lhs) == get_case_distance(base_case, rhs)){
      return lhs < rhs;
    }
    return get_case_distance(base_case, lhs) < get_case_distance(base_case, rhs);
  }
  int base_case;
};

struct rank_operator{
  bool operator ()(int lhs, int rhs)const{ 
    if(lhs == rhs){
      return false;
    }
    if(get_rank(lhs) == get_rank(rhs)){
      return lhs > rhs;
    }
    return get_rank(lhs) > get_rank(rhs);
  }
};
