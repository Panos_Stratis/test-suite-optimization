int printtokens2_main(int, char **);

#include <chrono>
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <string>
#include <stdio.h>
#include <sys/types.h>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

#include "tso.h"
#include "tso_cases.h"
#include "tso_tstcaseord.h"

//TODO Specify the following parameters.
#define MAX_NUM_THREAD 100
#define SHRD_MEM_SIZE 131072//in bytes
#define AVG_INSTR_SIZE 4//in bytes
#define L1_INSTR_CACHE_SIZE 32000//in bytes
#define LOW_THRESHOLD 0.25

#define PLAIN 1
#define RANDOM 2
#define OPTIMIZATION 3
#define OVERHEAD 4

std::map<std::string, int> const mode = {{"plain", PLAIN}, {"random", RANDOM}, {"optimization", OPTIMIZATION}, {"overhead", OVERHEAD}};

int execution_per_test_case;

float threshold;

std::string EXECUTION_CMD = "./";

std::string PINTOOL="obj-intel64/tso_bbltrack.so";

boost::mutex mtx;

std::vector<unsigned long long> executed_instructions;

std::unordered_set<std::size_t> basic_block_universe;

std::map<int, std::vector<size_t> > case_basic_block_map;

std::map<std::pair<int, int>, float> case_distances;

std::map<int, std::set<int,distance_operator> > case_similarity_map;

std::map<int, int> case_rank;

std::set<int, rank_operator> case_rank_set;

int get_rank(int case1){
  if(case_rank.count(case1) == 0){
    return 0;
  }
  return case_rank[case1];
}

float get_case_distance(int case1, int case2){
  if(case1 == case2){
    return 0.0;
  }
  if(case1 > case2){
    return case_distances.at(std::make_pair(case2, case1));
  }
  return case_distances.at(std::make_pair(case1, case2));
}

float calculate_case_distance(std::vector<std::size_t> *case1, std::vector<std::size_t> *case2){
  if(basic_block_universe.size() == 0){
    return 0.0;
  }

  std::vector<size_t> distance(basic_block_universe.size());
  std::vector<size_t>::iterator it;

  it = std::set_symmetric_difference(case1->begin(), case1->end(), case2->begin(), case2->end(), distance.begin());
  distance.resize(it-distance.begin());

  return (float)distance.size()/(float)basic_block_universe.size();
}

std::string construct_test_case_command(std::vector<std::string> execution_commands){
  std::string cmd = EXECUTION_CMD;
  for(auto const &s : execution_commands){
    if(&s != &execution_commands[0]){
      cmd += " "; 
    }
    cmd += s;
  }
  return cmd;
}

std::string construct_pin_command(std::string memory_id, std::string memory_set_id, std::string instruction_number_id, std::string case_cmd){
  std::string pin_cmd = "pin -t "+PINTOOL+" -mi "+memory_id+" -msi "+memory_set_id+" -ini "+instruction_number_id+" -- "+case_cmd;
  return pin_cmd;
}

void analysis_task(int case_id, std::string case_cmd){
  std::vector<size_t> case_basic_block_vector;
  std::string memory_id = std::to_string(case_id);
  std::string memory_set_id = memory_id+"set";
  std::string instruction_number_id = memory_id+"inst";

  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  boost::interprocess::managed_shared_memory segment(boost::interprocess::create_only, memory_id.c_str(), SHRD_MEM_SIZE);
  tso::shared_memory_set_allocator allocator(segment.get_segment_manager());
  tso::shared_memory_set* smset = segment.construct<tso::shared_memory_set>(memory_set_id.c_str())(std::less<std::size_t>(), allocator);
  unsigned long long* instruction_number = segment.construct<unsigned long long>(instruction_number_id.c_str())();

  std::system(construct_pin_command(memory_id, memory_set_id, instruction_number_id, case_cmd).c_str());
  
  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  mtx.lock();
  executed_instructions.push_back(*instruction_number);
  for (auto it=smset->begin(); it!=smset->end(); ++it){
    basic_block_universe.insert(*it);
    case_basic_block_vector.push_back(*it);
  }
  case_basic_block_map.insert(std::pair<int, std::vector<std::size_t>>(case_id, case_basic_block_vector));
  mtx.unlock();
}

void analyze_cases(){
  int thread_num = 0;
  if(test_cases.size() < MAX_NUM_THREAD){
    thread_num = test_cases.size();
  }
  else{
    thread_num = MAX_NUM_THREAD;
  }

  boost::asio::io_service ioService;
  for(auto const &case_entry : test_cases){
    ioService.post(boost::bind(analysis_task, case_entry.first, construct_test_case_command(case_entry.second)));
  }

  boost::thread_group thread_pool;
  for(int i=0; i<thread_num; i++){
    thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
  }
  thread_pool.join_all();
}

void update_similarity_map(int case1, int case2){
  if(case_rank.count(case1) == 0){
    distance_operator dop(case1);
    std::set<int, distance_operator> case_similarity_set(dop);
    case_similarity_set.insert(case2);
    case_similarity_map.insert(std::make_pair(case1, case_similarity_set));
  }
  case_rank[case1]++;
  case_similarity_map.at(case1).insert(case2);
}

void case_distance_task(int case1, std::vector<size_t> *case1_basic_blocks, int case2, std::vector<size_t> *case2_basic_blocks){
  if(case1 == case2){
    throw 0;
  }
  if(case1 > case2){
    std::swap(case1, case2);
  }

  float dist = calculate_case_distance(case1_basic_blocks, case2_basic_blocks);
  
  if(dist <= threshold){
    mtx.lock();
    case_distances.insert(std::make_pair(std::make_pair(case1, case2), dist));
    update_similarity_map(case1, case2);
    update_similarity_map(case2, case1);
    mtx.unlock();
  }
}

void calculate_threshold(){
  float cache_instruction_capacity = (float) L1_INSTR_CACHE_SIZE / (float) AVG_INSTR_SIZE;
  float average_instructions_executed;

  if(executed_instructions.size() == 0){
    average_instructions_executed = 0;
  }
  else{
    average_instructions_executed = std::accumulate(executed_instructions.begin(), executed_instructions.end(), 0.0 ) / executed_instructions.size();
  }

  threshold = 1 - (average_instructions_executed / cache_instruction_capacity);

  if(threshold < LOW_THRESHOLD){
    threshold = LOW_THRESHOLD;
  }
}

void calculate_case_distances(){
  int iteration_comparisons = case_basic_block_map.size();
  int thread_num = 0;
  calculate_threshold();
  for(auto it=case_basic_block_map.begin(); it!=case_basic_block_map.end(); ++it ){
    boost::asio::io_service ioService;
    boost::thread_group thread_pool;
    for(auto it2 = std::next(it); it2!=case_basic_block_map.end(); ++it2){
      ioService.post(boost::bind(case_distance_task, it->first, &it->second, it2->first, &it2->second));
    }
    iteration_comparisons--;
    
    if(iteration_comparisons < MAX_NUM_THREAD){
      thread_num = iteration_comparisons;
    }
    else{
      thread_num = MAX_NUM_THREAD;
    }
    for(int i=0; i<thread_num; i++){
      thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
    }
    thread_pool.join_all();
    case_rank_set.insert(it->first);
  }
  //basic_block_universe.clear();
  //case_basic_block_map.clear();
}

void visit_test_case(int test_case){
  test_order.push_back(test_case);
  case_rank_set.erase(test_case);
  if(case_rank[test_case] > 0){
    for(auto it=case_similarity_map.at(test_case).begin(); it!=case_similarity_map.at(test_case).end(); ++it){
      if(case_rank_set.count(*it) != 0){
	visit_test_case(*it);
	break;
      }
    }
  }
}

void calculate_case_order(){
  test_order.clear();
  while(!case_rank_set.empty()){
    int starting_case = *(case_rank_set.begin());
    visit_test_case(starting_case);
  }
  //case_distances.clear();
  //case_similarity_map.clear();
  //case_rank.clear();
  //case_rank_set.clear();
}

void optimize_test_suite(){
  std::chrono::system_clock::time_point start;
  std::chrono::system_clock::time_point end;
  
  start = std::chrono::system_clock::now();
  analyze_cases();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test cases analysis: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;

  start = std::chrono::system_clock::now();
  calculate_case_distances();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case distance calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
  
  start = std::chrono::system_clock::now();
  calculate_case_order();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case order calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;




  float val = 0;
  std::vector<float> dists;
  for(auto it = case_distances.begin(); it != case_distances.end(); it++) {
    dists.push_back(it->second);
  }
  for (std::vector<float>::iterator it = dists.begin() ; it != dists.end(); ++it){
    val += *it;
  }
  std::cerr<<"Average test case distance: "<<(val/dists.size())<<std::endl;

  float met = 0;
  std::vector<float> dista;
  for(auto it = case_basic_block_map.begin();it!=case_basic_block_map.end(); it++) {
    dista.push_back(((float)it->second.size() / basic_block_universe.size()));
  }
  for (std::vector<float>::iterator it = dista.begin() ; it != dista.end(); ++it){
    met += *it;
  }
  std::cerr<<"Average test case coverage:"<<(met/dista.size())<<std::endl;
}

void shuffle_test_suite(){
  std::random_shuffle(test_order.begin(), test_order.end());
}

void run_test_suite(){
  std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
  std::vector<std::string> test_arguments;
  for (std::vector<int>::iterator it = test_order.begin() ; it != test_order.end(); ++it){
    test_arguments = test_cases.at(*it);
    char **test  = new char *[test_arguments.size()];
    for(int i=0; i<test_arguments.size(); i++){
      test[i] = new char[test_arguments[i].size() + 1];
      std::strcpy(test[i], test_arguments[i].c_str());
    }
    for(int i=0; i<execution_per_test_case; i++){
      printtokens2_main(test_arguments.size(), test);
    }
    for(int i=0; i<test_arguments.size(); i++){
      delete test[i];
      test[i] = NULL;
    }
    delete[] test;
    test = NULL;
  }
  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  std::cerr<<"Test suite run : "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

int main(int argc, char * argv[]){
  if(argc < 2){
    std::cerr<<"No execution mode was selected [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  if(mode.count(std::string(argv[1])) == 0){
    std::cerr<<"Invalid execution mode [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  execution_per_test_case = std::atoi(argv[2]);

  switch(mode.at(std::string(argv[1]))){
  case PLAIN :
    run_test_suite();
    break;
  case RANDOM :
    shuffle_test_suite();
    run_test_suite();
    break;
  case OPTIMIZATION :
    optimize_test_suite();
    run_test_suite();
    break;
  case OVERHEAD :
    optimize_test_suite();
    break;
    }

  return 0;
}



/*
  =====================================================================================================================
 */




/***********************************************/
/*  assgnment.5  Shu Z. A00042813 for CS453    */
/*  using the tokenizer and stream module      */
/*  print_tokens.c Code                        */
/***********************************************/

/***********************************************/
/* NAME:	print_tokens                   */
/* INPUT:	a filename                     */
/* OUTPUT:      print out the token stream     */
/* DESCRIPTION: using the tokenizer interface  */
/*              to print out the token stream  */
/***********************************************/
//#include <stdio.h>
#include <cstdio>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "tokens.h"
#define TRUE 1
#define FALSE 0

 int is_identifier(token);
int is_eof_token(token);
 int is_keyword(token);
 int is_char_constant(token);
 int is_num_constant(token);
 void print_spec_symbol(token);
 int is_spec_symbol(token);
 void  unget_error(character_stream *);
 int is_comment(token);
 int is_str_constant(token);
char unget_char(character_stream,char);
 void  unget_error(character_stream );
int is_token_end(int,char);
int print_token(token);
token get_token(token_stream);
token_stream open_token_stream(char *);
char unget_char(character_stream ,char);

int printtokens2_main(int argc, char * argv[])
{  char *fname;
   token tok;
   token_stream tp;
     if(argc==1)                  /* if not given filename,take as '""' */
       {
        fname= (char *) malloc(sizeof(char));
        *fname = '\0'; 
       }
     else if(argc==2)
        fname= argv[1];
     else
       { fprintf(stdout, "Error!,please give the token stream\n");
         return 0;
       }
    tp=open_token_stream(fname);  /* open token stream */
    if(tp != 0){
      tok=get_token(tp);
      while (is_eof_token(tok) ==FALSE) /* take one token each time until eof */
	{
	  print_token(tok);
	  tok=get_token(tp);
	}
      print_token(tok); /* print eof signal */
    }
    return 0;
}

/* stream.c code */

/***********************************************/
/* NMAE:	open_character_stream          */
/* INPUT:       a filename                     */
/* OUTPUT:      a pointer to chacracter_stream */
/* DESCRIPTION: when not given a filename,     */
/*              open stdin,otherwise open      */
/*              the existed file               */
/***********************************************/
character_stream open_character_stream(char * fname)
{ character_stream fp;
  if(fname == NULL)
     fp=stdin;
  else if ((fp=fopen(fname,"r"))== NULL)
  {
    fprintf(stdout, "The file %s doesn't exists\n",fname);
       return 0;
  }
  return(fp);
}

/********************************************************/
/* NAME:	open_token_stream                       */
/* INPUT:       a filename                              */
/* OUTPUT:      a pointer to a token_stream             */
/* DESCRIPTION: when filename is EMPTY,choice standard  */
/*              input device as input source            */
/********************************************************/
token_stream open_token_stream(char *fname)
{
 token_stream fp;
 if(strcmp(fname,"")==0)
    fp=open_character_stream(NULL);
 else
    fp=open_character_stream(fname);
 return(fp);
}

/**********************************************/
/* NAME:	get_char                      */
/* INPUT:       a pointer to char_stream      */
/* OUTPUT:      a character                   */
/**********************************************/
char get_char(character_stream fp)
{ char ch;
  ch=getc(fp);
  return(ch);
}

/***************************************************/
/* NAME:      unget_char                           */
/* INPUT:     a pointer to char_stream,a character */
/* OUTPUT:    a character                          */
/* DESCRIPTION:when unable to put back,return EOF  */
/***************************************************/
char unget_char(character_stream ch,char fp)
{ char c;
  c=ungetc(fp, ch);
  if(c ==EOF)
    {
     return(c);
    }
  else
     return(c);
}

/* tokenizer.c code */


char buffer[81];  /* fixed array length MONI */ /* to store the token temporar */


 int is_spec_symbol();
 int is_token_end();
 int unget_error();
 int is_keyword();
 int is_identifier();
 int is_num_constant();
 int is_char_constant();
 int is_str_constant();
 int is_comment();
 void print_spec_symbol();



/********************************************************/
/* NAME :	get_token                               */
/* INPUT: 	a pointer to the tokens_stream          */
/* OUTPUT:      a token                                 */
/* DESCRIPTION: according the syntax of tokens,dealing  */
/*              with different case  and get one token  */
/********************************************************/
token get_token(token_stream tp)
{ 
  int i=0,j;
  int id=0;
  char ch,ch1[2];
  for (j=0;j<=80;j++)          /* initial the buffer   */
      { buffer[j]='\0';} 
   ch1[0]='\0';
   ch1[1]='\0';
   ch=get_char(tp);
   while(ch==' '||ch=='\n')      /* strip all blanks until meet characters */
      {
       ch=get_char(tp);
      } 
   buffer[i]=ch;
   if(is_eof_token(buffer)==TRUE)return(buffer);
   if(is_spec_symbol(buffer)==TRUE)return(buffer); 
   if(ch =='"')id=1;    /* prepare for string */
   if(ch ==59)id=2;    /* prepare for comment */
   ch=get_char(tp);

   while (is_token_end(id,ch) == FALSE)/* until meet the end character */
   {
       i++;
       buffer[i]=ch;
       ch=get_char(tp);
   }
   ch1[0]=ch;                        /* hold the end charcater          */
   if(is_eof_token(ch1)==TRUE)       /* if end character is eof token    */
     { ch=unget_char(tp, ch);        /* then put back eof on token_stream */
        if(ch==EOF)unget_error();
        return(buffer);
      }
   if(is_spec_symbol(ch1)==TRUE)     /* if end character is special_symbol */
     { ch=unget_char(tp, ch);        /* then put back this character       */
        if(ch==EOF)unget_error();
        return(buffer);
      }
   if(id==1)                  /* if end character is " and is string */
     { i++;                     /* case,hold the second " in buffer    */
       buffer[i]=ch;
       return(buffer); 
     }
  return(buffer);                   /* return nomal case token             */
}

/*******************************************************/
/* NAME:	is_token_end                           */
/* INPUT:       a character,a token status             */
/* OUTPUT:	a BOOLEAN value                        */
/*******************************************************/
 int is_token_end(int str_com_id,char ch)
{ char ch1[2];  /* fixed array declaration MONI */
 ch1[0]=ch;
 ch1[1]='\0';
 if(is_eof_token(ch1)==TRUE)return(TRUE); /* is eof token? */
 if(str_com_id==1)          /* is string token */
    { if(ch=='"' | ch=='\n')   /* for string until meet another " */
         return(TRUE);
      else
         return(FALSE);
    }

 if(str_com_id==2)    /* is comment token */
   { if(ch=='\n')     /* for comment until meet end of line */
        return(TRUE);
      else
        return(FALSE);
   }

 if(is_spec_symbol(ch1)==TRUE) return(TRUE); /* is special_symbol? */
 if(ch ==' ' || ch=='\n' || ch==59) return(TRUE); 
                              /* others until meet blank or tab or 59 */
 return(FALSE);               /* other case,return FALSE */
}

/****************************************************/
/* NAME :	token_type                          */
/* INPUT:       a pointer to the token              */
/* OUTPUT:      an integer value                    */
/* DESCRIPTION: the integer value is corresponding  */
/*              to the different token type         */
/****************************************************/
 int token_type(token tok)
{ 
 if(is_keyword(tok))return(keyword);
 if(is_spec_symbol(tok))return(spec_symbol);
 if(is_identifier(tok))return(identifier);
 if(is_num_constant(tok))return(num_constant);
 if(is_str_constant(tok))return(str_constant);
 if(is_char_constant(tok))return(char_constant);
 if(is_comment(tok))return(comment);
 if(is_eof_token(tok))return(end);
 return(error);                    /* else look as error token */
}

/****************************************************/
/* NAME:	print_token                         */
/* INPUT:	a pointer to the token              */
/* OUTPUT:      a BOOLEAN value,print out the token */
/*              according the forms required        */
/****************************************************/
int print_token(token tok)
{ int type;
  type=token_type(tok);
 if(type==error)
   { fprintf(stdout, "error\n");
   } 
 if(type==keyword)
   {fprintf(stdout, "keyword,\"%s\".\n",tok);
   }
 if(type==spec_symbol)print_spec_symbol(tok);
 if(type==identifier)
   {fprintf(stdout, "identifier,\"%s\".\n",tok);
   }
 if(type==num_constant)
   {fprintf(stdout, "numeric,%s.\n",tok);
   }
 if(type==str_constant)
   {fprintf(stdout, "string,%s.\n",tok);
   }
 if(type==char_constant)
   {tok=tok+1;
    fprintf(stdout, "character,\"%s\".\n",tok);
   }
 if(type==end) 
   fprintf(stdout, "eof.\n");
 return 0;
}

/* the code for tokens judgment function */

/*************************************/
/* NAME:	is_eof_token         */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
int is_eof_token(token tok)
{ 
  if( *tok==EOF)
      return(TRUE);
  else
      return(FALSE);
}

/*************************************/
/* NAME:	is_comment           */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_comment(token ident)
{
  if( (*ident) ==59 )   /* the char is 59   */
     return(TRUE);
  else
     return(FALSE);
}

/*************************************/
/* NAME:	is_keyword           */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_keyword(token str)
{ 
 if (!strcmp(str,"and") || !strcmp(str,"or") || !strcmp(str,"if") ||
    !strcmp(str,"xor")||!strcmp(str,"lambda")||!strcmp(str,"=>"))
      return(TRUE);
  else 
      return(FALSE);
}

/*************************************/
/* NAME:	is_char_constant     */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_char_constant(token str)
{
  if ((*str)=='#' && isalpha(*(str+1)))
     return(TRUE);
  else  
     return(FALSE);
}

/*************************************/
/* NAME:	is_num_constant      */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_num_constant(token str)
{
  int i=1;
  
  if ( isdigit(*str)) 
    {
    while ( *(str+i) != '\0' )   /* until meet token end sign */
      {
       if(isdigit(*(str+i)))
         i++;
       else
         return(FALSE);
      }                         /* end WHILE */
    return(TRUE);
    }
  else
   return(FALSE);               /* other return FALSE */
}

/*************************************/
/* NAME:	is_str_constant      */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_str_constant(token str)
{
  int i=1;
 
  if ( *str =='"')
     { while (*(str+i)!='\0')  /* until meet the token end sign */
         { if(*(str+i)=='"')
             return(TRUE);        /* meet the second '"'           */
           else
           i++;
         }               /* end WHILE */
     return(FALSE);
    }
  else
    return(FALSE);       /* other return FALSE */
}
/*************************************/
/* NAME:	is_identifier         */
/* INPUT: 	a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_identifier(token str)
{
  int i=1;

  if ( isalpha( *str) ) 
     {
        while(  *(str+i) !='\0' )   /* unti meet the end token sign */
           { 
            if(isalpha(*(str+i)) || isdigit(*(str+i)))   
               i++;
            else
               return(FALSE);
           }      /* end WHILE */
     return(TRUE);
     }
  else
     return(FALSE);
}

/******************************************/
/* NAME:	unget_error               */
/* INPUT:       a pointer to token stream */
/* OUTPUT: 	print error message       */
/******************************************/
 void  unget_error(character_stream *fp)
{
fprintf(stdout,"It can not get charcter\n");
}

 int  unget_error()
{
fprintf(stdout,"It can not get charcter\n");
 return 0;
}

/*************************************************/
/* NAME:        print_spec_symbol                */
/* INPUT:       a pointer to a spec_symbol token */
/* OUTPUT :     print out the spec_symbol token  */
/*              according to the form required   */
/*************************************************/
 void print_spec_symbol(token str)
{
    if      (!strcmp(str,"("))
    {
             fprintf(stdout, "%s\n","lparen.");
             return;
    } 
    if (!strcmp(str,")"))
    {
             fprintf(stdout, "%s\n","rparen.");
             return;
    }
    if (!strcmp(str,"["))
    {
             fprintf(stdout, "%s\n","lsquare.");
             return;
    }
    if (!strcmp(str,"]"))
    {
             fprintf(stdout, "%s\n","rsquare.");
             return;
    }
    if (!strcmp(str,"'"))
    {
             fprintf(stdout, "%s\n","quote.");
             return;
    }
    if (!strcmp(str,"`"))
    {
             fprintf(stdout, "%s\n","bquote.");
             return;
    }
    
             fprintf(stdout, "%s\n","comma.");
}


/*************************************/
/* NAME:        is_spec_symbol       */
/* INPUT:       a pointer to a token */
/* OUTPUT:      a BOOLEAN value      */
/*************************************/
 int is_spec_symbol(token str)
{
    if (!strcmp(str,"("))
    {  
        return(TRUE);
    }
    if (!strcmp(str,")"))
    {
        return(TRUE);
    }
    if (!strcmp(str,"["))
    {
        return(TRUE);
    }
    if (!strcmp(str,"]"))
    {
        return(TRUE);
    }
    if (!strcmp(str,"'"))
    {
        return(TRUE);
    }
    if (!strcmp(str,"`"))
    {
        return(TRUE);
    }
    if (!strcmp(str,","))
    {
        return(TRUE);
    }
    return(FALSE);     /* others return FALSE */
}


