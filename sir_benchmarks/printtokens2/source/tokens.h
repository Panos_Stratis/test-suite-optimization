/* tokens.h code */

#include <cstdio> 
#include "stream.h"
#define error 0
#define keyword 1
#define spec_symbol 2
#define identifier 3
#define num_constant 41
#define str_constant 42
#define char_constant 43
#define comment 5
#define end 6

typedef
    FILE *token_stream;
typedef   
    char *token;

extern token_stream open_token_stream();
extern token get_token();
extern int print_token();
extern int is_eof_token();
extern int compare_token();

int is_token_end();
 int token_type();
 int is_comment();
 int is_keyword();
 int is_char_constant();
 int is_num_constant();
 int is_str_constant();
 int is_identifier();
 int is_spec_symbol();
 int unget_error();
 void print_spec_symbol();

