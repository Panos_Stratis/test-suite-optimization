/*
  The test cases and their execution order.

  Entry in test_cases: {<test_id>, {<executable_name>, <argument_1>, <argument_2>, ..., <argument_n>}}

  Entry in test_order: <test1_id>

  Example: 

  std::map<int, std::vector<std::string>> const test_cases { {0, {"utest","1", "1"} }, {1, {"utest", "11", "1"} }, {2, {"utest", "1", "11"} }, {3, {"utest", "1", "1"} }, {4,{"utest", "1", "1"}  } };

  std::vector<int> test_order = {0, 1, 2, 3, 4};
  
 */
#include <map>

std::map<int, std::vector<std::string>> const test_cases {  };

std::vector<int> test_order = {  };
