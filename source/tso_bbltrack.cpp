#include "pin.H"
#include "tso.h"
#include "tso_bbltrack.h"
#include <boost/interprocess/managed_shared_memory.hpp>
#include <iostream>

std::hash<std::string> hash_function;

tso::shared_memory_set *smset;

unsigned long long *instruction_num;

KNOB<std::string> memory_id(KNOB_MODE_WRITEONCE, "pintool", "mi", "", "Specify memory id.");

KNOB<std::string> memory_set_id(KNOB_MODE_WRITEONCE, "pintool", "msi", "", "Specify memory set id.");

KNOB<std::string> instruction_number_id(KNOB_MODE_WRITEONCE, "pintool", "ini", "", "Specify memory set id.");

void record_basic_block(std::size_t basic_block_id, UINT32 basic_block_instruction_number){
  if(smset->insert(basic_block_id).second == true){
    *(instruction_num) += basic_block_instruction_number;
  }
}

std::size_t construct_signature(std::string *file_name, INT32 *line_number){
  return hash_function((*file_name)+std::to_string(*line_number));
}

VOID process_basic_block(ADDRINT basic_block_address, UINT32 basic_block_instruction_number){
  std::string file_name;
  INT32 line_number=0;
  
  PIN_LockClient();
  //No need for column information, just pass NULL.
  PIN_GetSourceLocation(basic_block_address, NULL, &line_number, &file_name);  
  PIN_UnlockClient();

  //Record only blocks for which there is valid line and file name information.  
  if((line_number != 0) && (!file_name.empty())){    
    record_basic_block(construct_signature(&file_name, &line_number), basic_block_instruction_number);
  }
}

VOID Trace(TRACE trace, VOID *v){
  for (BBL basic_block = TRACE_BblHead(trace); BBL_Valid(basic_block); basic_block = BBL_Next(basic_block)){
    BBL_InsertCall(basic_block, IPOINT_BEFORE, (AFUNPTR)process_basic_block, IARG_ADDRINT, BBL_Address(basic_block), IARG_UINT32, BBL_NumIns(basic_block), IARG_END);
  }
}

int main(int argc, char * argv[]){
  if (PIN_Init(argc, argv)){
    std::cerr<<"Failed to initialize pin. Exiting..."<<std::endl;
    return -1;
  }

  boost::interprocess::managed_shared_memory segment(boost::interprocess::open_only, memory_id.Value().c_str());
  
  smset = segment.find<tso::shared_memory_set>(memory_set_id.Value().c_str()).first;

  instruction_num = segment.find<unsigned long long>(instruction_number_id.Value().c_str()).first;

  *(instruction_num) = 0;

  TRACE_AddInstrumentFunction(Trace, 0);
    
  PIN_InitSymbols();

  PIN_StartProgram();
    
  return 0;
}
