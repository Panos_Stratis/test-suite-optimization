/*
  Under analysis software code must be merged with this tool.

  The main of the under analysis software should be renamed to analysis_main .  
 */

#include <chrono>
#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <unordered_set>
#include <string>
#include <stdio.h>
#include <sys/types.h>

#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

#include "tso.h"
#include "tso_cases.h"
#include "tso_tstcaseord.h"

//TODO Specify the following parameters.
#define MAX_NUM_THREAD 100
#define SHRD_MEM_SIZE 131072//in bytes
#define AVG_INSTR_SIZE 4//in bytes
#define L1_INSTR_CACHE_SIZE 32000//in bytes
#define LOW_THRESHOLD 0.25

#define PLAIN 1
#define RANDOM 2
#define OPTIMIZATION 3
#define OVERHEAD 4

std::map<std::string, int> const mode = {{"plain", PLAIN}, {"random", RANDOM}, {"optimization", OPTIMIZATION}, {"overhead", OVERHEAD}};

int execution_per_test_case;

float threshold;

std::string EXECUTION_CMD = "./";

std::string PINTOOL="obj-intel64/tso_bbltrack.so";

boost::mutex mtx;

std::vector<unsigned long long> executed_instructions;

std::unordered_set<std::size_t> basic_block_universe;

std::map<int, std::vector<size_t> > case_basic_block_map;

std::map<std::pair<int, int>, float> case_distances;

std::map<int, std::set<int,distance_operator> > case_similarity_map;

std::map<int, int> case_rank;

std::set<int, rank_operator> case_rank_set;

int get_rank(int case1){
  if(case_rank.count(case1) == 0){
    return 0;
  }
  return case_rank[case1];
}

float get_case_distance(int case1, int case2){
  if(case1 == case2){
    return 0.0;
  }
  if(case1 > case2){
    return case_distances.at(std::make_pair(case2, case1));
  }
  return case_distances.at(std::make_pair(case1, case2));
}

float calculate_case_distance(std::vector<std::size_t> *case1, std::vector<std::size_t> *case2){
  if(basic_block_universe.size() == 0){
    return 0.0;
  }

  std::vector<size_t> distance(basic_block_universe.size());
  std::vector<size_t>::iterator it;

  it = std::set_symmetric_difference(case1->begin(), case1->end(), case2->begin(), case2->end(), distance.begin());
  distance.resize(it-distance.begin());

  return (float)distance.size()/(float)basic_block_universe.size();
}

std::string construct_test_case_command(std::vector<std::string> execution_commands){
  std::string cmd = EXECUTION_CMD;
  for(auto const &s : execution_commands){
    if(&s != &execution_commands[0]){
      cmd += " "; 
    }
    cmd += s;
  }
  return cmd;
}

std::string construct_pin_command(std::string memory_id, std::string memory_set_id, std::string instruction_number_id, std::string case_cmd){
  std::string pin_cmd = "pin -t "+PINTOOL+" -mi "+memory_id+" -msi "+memory_set_id+" -ini "+instruction_number_id+" -- "+case_cmd;
  return pin_cmd;
}

void analysis_task(int case_id, std::string case_cmd){
  std::vector<size_t> case_basic_block_vector;
  std::string memory_id = std::to_string(case_id);
  std::string memory_set_id = memory_id+"set";
  std::string instruction_number_id = memory_id+"inst";

  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  boost::interprocess::managed_shared_memory segment(boost::interprocess::create_only, memory_id.c_str(), SHRD_MEM_SIZE);
  tso::shared_memory_set_allocator allocator(segment.get_segment_manager());
  tso::shared_memory_set* smset = segment.construct<tso::shared_memory_set>(memory_set_id.c_str())(std::less<std::size_t>(), allocator);
  unsigned long long* instruction_number = segment.construct<unsigned long long>(instruction_number_id.c_str())();

  std::system(construct_pin_command(memory_id, memory_set_id, instruction_number_id, case_cmd).c_str());
  
  boost::interprocess::shared_memory_object::remove(memory_id.c_str());
  
  mtx.lock();
  executed_instructions.push_back(*instruction_number);
  for (auto it=smset->begin(); it!=smset->end(); ++it){
    basic_block_universe.insert(*it);
    case_basic_block_vector.push_back(*it);
  }
  case_basic_block_map.insert(std::pair<int, std::vector<std::size_t>>(case_id, case_basic_block_vector));
  mtx.unlock();
}

void analyze_cases(){
  int thread_num = 0;
  if(test_cases.size() < MAX_NUM_THREAD){
    thread_num = test_cases.size();
  }
  else{
    thread_num = MAX_NUM_THREAD;
  }

  boost::asio::io_service ioService;
  for(auto const &case_entry : test_cases){
    ioService.post(boost::bind(analysis_task, case_entry.first, construct_test_case_command(case_entry.second)));
  }

  boost::thread_group thread_pool;
  for(int i=0; i<thread_num; i++){
    thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
  }
  thread_pool.join_all();
}

void update_similarity_map(int case1, int case2){
  if(case_rank.count(case1) == 0){
    distance_operator dop(case1);
    std::set<int, distance_operator> case_similarity_set(dop);
    case_similarity_set.insert(case2);
    case_similarity_map.insert(std::make_pair(case1, case_similarity_set));
  }
  case_rank[case1]++;
  case_similarity_map.at(case1).insert(case2);
}

void case_distance_task(int case1, std::vector<size_t> *case1_basic_blocks, int case2, std::vector<size_t> *case2_basic_blocks){
  if(case1 == case2){
    throw 0;
  }
  if(case1 > case2){
    std::swap(case1, case2);
  }

  float dist = calculate_case_distance(case1_basic_blocks, case2_basic_blocks);
  
  if(dist <= threshold){
    mtx.lock();
    case_distances.insert(std::make_pair(std::make_pair(case1, case2), dist));
    update_similarity_map(case1, case2);
    update_similarity_map(case2, case1);
    mtx.unlock();
  }
}

void calculate_threshold(){
  float cache_instruction_capacity = (float) L1_INSTR_CACHE_SIZE / (float) AVG_INSTR_SIZE;
  float average_instructions_executed;

  if(executed_instructions.size() == 0){
    average_instructions_executed = 0;
  }
  else{
    average_instructions_executed = std::accumulate(executed_instructions.begin(), executed_instructions.end(), 0.0 ) / executed_instructions.size();
  }

  threshold = 1 - (average_instructions_executed / cache_instruction_capacity);

  if(threshold < LOW_THRESHOLD){
    threshold = LOW_THRESHOLD;
  }
}

void calculate_case_distances(){
  int iteration_comparisons = case_basic_block_map.size();
  int thread_num = 0;
  calculate_threshold();
  for(auto it=case_basic_block_map.begin(); it!=case_basic_block_map.end(); ++it ){
    boost::asio::io_service ioService;
    boost::thread_group thread_pool;
    for(auto it2 = std::next(it); it2!=case_basic_block_map.end(); ++it2){
      ioService.post(boost::bind(case_distance_task, it->first, &it->second, it2->first, &it2->second));
    }
    iteration_comparisons--;
    
    if(iteration_comparisons < MAX_NUM_THREAD){
      thread_num = iteration_comparisons;
    }
    else{
      thread_num = MAX_NUM_THREAD;
    }
    for(int i=0; i<thread_num; i++){
      thread_pool.create_thread(boost::bind(&boost::asio::io_service::run, &ioService));
    }
    thread_pool.join_all();
    case_rank_set.insert(it->first);
  }
  basic_block_universe.clear();
  case_basic_block_map.clear();
}

void visit_test_case(int test_case){
  test_order.push_back(test_case);
  case_rank_set.erase(test_case);
  if(case_rank[test_case] > 0){
    for(auto it=case_similarity_map.at(test_case).begin(); it!=case_similarity_map.at(test_case).end(); ++it){
      if(case_rank_set.count(*it) != 0){
	visit_test_case(*it);
	break;
      }
    }
  }
}

void calculate_case_order(){
  test_order.clear();
  while(!case_rank_set.empty()){
    int starting_case = *(case_rank_set.begin());
    visit_test_case(starting_case);
  }
  case_distances.clear();
  case_similarity_map.clear();
  case_rank.clear();
  case_rank_set.clear();
}

void optimize_test_suite(){
  std::chrono::system_clock::time_point start;
  std::chrono::system_clock::time_point end;
  
  start = std::chrono::system_clock::now();
  analyze_cases();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case analysis: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;

  start = std::chrono::system_clock::now();
  calculate_case_distances();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case distance calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
  
  start = std::chrono::system_clock::now();
  calculate_case_order();
  end = std::chrono::system_clock::now();
  std::cerr<<"Test case order calculation: "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

void shuffle_test_suite(){
  std::random_shuffle(test_order.begin(), test_order.end());
}

void run_test_suite(){
  std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
  std::vector<std::string> test_arguments;
  for (std::vector<int>::iterator it = test_order.begin() ; it != test_order.end(); ++it){
    test_arguments = test_cases.at(*it);
    char **test  = new char *[test_arguments.size()];
    for(int i=0; i<test_arguments.size(); i++){
      test[i] = new char[test_arguments[i].size() + 1];
      std::strcpy(test[i], test_arguments[i].c_str());
    }
    for(int i=0; i<execution_per_test_case; i++){
      //Call the main of the under analysis software here.
      analysis_main(test_arguments.size(), test);
    }
    for(int i=0; i<test_arguments.size(); i++){
      delete test[i];
      test[i] = NULL;
    }
    delete[] test;
    test = NULL;
  }
  std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
  std::cerr<<"Test suite run : "<<std::chrono::duration_cast<std::chrono::seconds>(end-start).count()<<" seconds."<<std::endl;
}

int main(int argc, char * argv[]){
  if(argc < 2){
    std::cerr<<"No execution mode was selected [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  if(mode.count(std::string(argv[1])) == 0){
    std::cerr<<"Invalid execution mode [plain - random - optimization - overhead]. Exiting..."<<std::endl;
    return -1;
  }

  execution_per_test_case = std::atoi(argv[2]);

  switch(mode.at(std::string(argv[1]))){
  case PLAIN :
    run_test_suite();
    break;
  case RANDOM :
    shuffle_test_suite();
    run_test_suite();
    break;
  case OPTIMIZATION :
    optimize_test_suite();
    run_test_suite();
    break;
  case OVERHEAD :
    optimize_test_suite();
    break;
    }

  return 0;
}
