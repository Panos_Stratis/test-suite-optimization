void record_basic_block(std::size_t, UINT32);

std::size_t construct_signature(std::string *, INT32 *);

VOID process_basic_block(ADDRINT, UINT32);

VOID Trace(TRACE, VOID *);

int main(int, char **);
